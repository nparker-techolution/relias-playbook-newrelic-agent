GALAXY	:= $(command -v ansible-galaxy || true)

all: deps site


$(GALAXY):
	@curl https://sh.rustup.rs -sSf | sh
	@pip3 install -r requirements.txt

lint:
	@yamllint $(wildcard *.yml)

deps: $(GALAXY)
	@ansible-galaxy install -r requirements.yml

graph:
	@ansible-inventory --$@

relias-dev relias-prod relias-staging site:
	@ansible-playbook $@.yml
